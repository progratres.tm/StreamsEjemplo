import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main 
{
	public static void main(String[] args) 
	{
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8); 
		List<Integer> twoEvenSquares = 
		numbers.stream()    
		.filter(n -> {     
		System.out.println("filtering " + n);   
		return n % 2 == 0;    
		})    
		.map(n -> {     
		System.out.println("mapping " + n);     
		return n * n;    
		})    
		.limit(2)
		.collect(Collectors.toList());
	
	for (int num : twoEvenSquares)
		System.out.println(num);

	
	ArrayList<Persona> personas = new ArrayList<Persona>() ;

	personas.add(new Persona("Ana",10, 50));
	personas.add(new Persona("Mia",11, 50));		
	personas.add(new Persona("Paula",12, 60));
	personas.add(new Persona("Manuel",28, 40));
	personas.add(new Persona("Sofia",22, 30));

	personas.stream().forEach(System.out::println);
	personas.stream().forEach(p -> p.mostrarNombre());

	//Formas equivalentes de escribir, desde la clase o desde la persona
	//int tot = personas.stream().mapToInt(Persona::getEdad).sum();
	int tot = personas.stream().mapToInt(p -> p.getEdad()).sum();
	System.out.println("la suma de las edades de todas las personas es: "+tot);

	OptionalDouble promedio = personas
			.stream()
			.mapToInt(Persona::getEdad)
			.average();

	System.out.println("El promedio de las edades de todas las personas es: " + promedio);

	OptionalDouble promedioPesoMayores20 = personas
			.stream()
			.filter(p->p.getEdad()>20)
			.mapToInt(Persona::getPeso)
			.average();

	System.out.println("El promedio del peso de las personas mayores de 20 a�os es: " + promedioPesoMayores20);

	System.out.println(" El promedio del peso de las personas mayores de 25 a�o es: " + 
			personas
			.stream()
			.filter(p->p.getEdad()>25)
			.mapToInt(Persona::getPeso)
			.average()
			);

	Stream<Integer> numeros = Stream.iterate(40, n -> n + 2).limit(5);
	numeros.forEach(System.out::println);

	IntStream
		.range(1, 10)
		.filter(x -> x % 2 == 0)
		.forEach(System.out::println);

	Random random = new Random();
	IntStream
		.iterate(random.nextInt(100), n -> n+1)
		.limit(20)
		.filter(n -> n % 3 == 0)
		.forEach(System.out::println);

	ArrayList<String>  paises = new ArrayList<String>();
	paises.add("Argentina");
	paises.add("Colombia");
	paises.add("Brasil");
	paises.add("Uruguay");
	paises.add("Chile");

	//Stream cuyos elementos son los pa�ses en may�sculas 
	paises
		.stream()
		.map(String::toUpperCase)
		.filter(s ->  s.charAt(0) == 'C')
		.forEach(System.out::println);

	paises
	.stream()
	.skip(2)
	.forEach(System.out::println);

	paises
	.stream()
	.limit(4)
	.map(String::toUpperCase)
	.forEach(System.out::println);
	}		
}


